<?php
    while ( have_posts() ) :
		the_post(); ?>

		<div class="card">
			<div class="card-content">
				<span class="card-title"><?php the_title() ?></span>
				<p>
					<small><?php the_date() ?></small>
				</p>

				<div class="separator separator-20"></div>

				<p>
					<?php the_excerpt() ?>
					<?php the_post_thumbnail_url('full'); ?>
				</p>
			</div>
			<div class="card-action">
				<a href="<?php the_permalink() ?>" class="btn btn-primary">Leia mais</a>
			</div>
		</div>

		<div class="separator separator-40"></div>

		<?php
    endwhile; ?>
    

    <div class="separator separator-40"></div>

    <?php
	echo get_the_posts_pagination(
		array(
			'mid_size' => 50,
			'prev_text' => __( 'Página anterior', 'textdomain' ),
			'next_text' => __( 'Próxima página', 'textdomain' ),
		)
    );
?>