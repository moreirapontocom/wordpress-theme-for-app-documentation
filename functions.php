<?php
function my_queue() {
	wp_enqueue_style('_materialize',get_template_directory_uri().'/assets/materialize/dist/css/materialize.min.css','','all');
	wp_enqueue_style('_fontawesome',get_template_directory_uri().'/assets/font-awesome/css/font-awesome.min.css','','all');
	wp_enqueue_style('_style',get_template_directory_uri().'/style.css',array('_materialize','_fontawesome'),'','all');

	wp_enqueue_script('jQuery',get_template_directory_uri().'/assets/jquery/dist/jquery.min.js','',true);
	wp_enqueue_script('_materializejs',get_template_directory_uri().'/assets/materialize/dist/js/materialize.min.js',array('jQuery'),'',true);
	wp_enqueue_script('_scripts',get_template_directory_uri().'/scripts.js',array('jQuery','_materializejs'),'',true);
}
add_action( 'wp_enqueue_scripts', 'my_queue' );

function custom_theme_setup() {
	add_theme_support('post-thumbnails');
	add_theme_support('widgets');
}
add_action( 'after_setup_theme', 'custom_theme_setup' );

register_sidebar(
	array(
		'name' => __( 'Sidebar', 'docs' ),
		'id' => 'main-sidebar',
		'description' => __( 'Barra auxiliar', 'docs' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<p>',
		'after_title' => '</p>',
	)
);

function remove_posts_menu() {
    remove_menu_page('edit.php?post_type=page');
}
add_action('admin_init', 'remove_posts_menu');

function getAppLink() {
	return 'https://app.truo.com.br/?utm_source=docs';
}

