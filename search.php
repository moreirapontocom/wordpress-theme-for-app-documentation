<?php get_header(); ?>

	<?php
	global $query_string;

	$query_args = explode("&", $query_string);
	$search_query = array();

	if ( strlen($query_string) > 0 ) {
		foreach ( $query_args as $key => $string ) {
			$query_split = explode("=", $string);
			$search_query[$query_split[0]] = urldecode($query_split[1]);
		}
	}

	$search = new WP_Query($search_query);
	?>

	Você buscou por: <strong><i><?php echo $search->query['s'] ?></i></strong>

	<div class="separator separator-30"></div>

	<?php get_template_part('loop-posts'); ?>

	<?php wp_reset_query(); ?>

<?php get_footer(); ?>