<?php get_header(); ?>

	<div class="row">
		<div class="col l8">

			<?php
			while ( have_posts() ) :
				the_post(); ?>

				<h4><?php the_title() ?></h4>
				<small><?php the_date() ?></small>

				<div class="separator separator-40"></div>

				<div class="card">
					<div class="card-content single">
						<?php the_content() ?>
					</div>
				</div>

				<div class="separator separator-40"></div>

				<?php
			endwhile; ?>

		</div>
		<div class="col l4">

			<?php if ( is_active_sidebar( 'main-sidebar' ) ) dynamic_sidebar( 'main-sidebar' ); ?>

		</div>
	</div>

<?php get_footer(); ?>
