<?php get_header(); ?>

	<div class="row">
		<div class="col l8">

			<?php
			$categories = get_queried_object();
			if ( isset($categories->cat_ID) && !empty($categories->cat_ID) ) {
				$category_id = $categories->cat_ID;
				$category_name = $categories->cat_name;
			} else {
				$category_id = 1;
				$category_name = 'Nenhuma categoria selecionada';
			}
			?>

			Categoria: <strong><i><?php echo $category_name ?></i></strong>

			<div class="separator separator-30"></div>

			<?php
			query_posts( 'post_type=post&cat='.$category_id );

			get_template_part('loop-posts');

			wp_reset_query();
			?>

		</div>
		<div class="col l4">

			<?php if ( is_active_sidebar( 'main-sidebar' ) ) dynamic_sidebar( 'main-sidebar' ); ?>

		</div>
	</div>

<?php get_footer(); ?>
