<?php get_header(); ?>

	<div class="row">
		<div class="col l8">

			<?php
			$query_args = array(
				'post_type' => 'post',
				'posts_per_page' => get_option( 'posts_per_page' ),
				'orderby' => 'date',
				'paged' => ( ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1 )
			);
			query_posts( $query_args );

			get_template_part('loop-posts');

			wp_reset_query(); ?>

		</div>
		<div class="col l4">

			<?php if ( is_active_sidebar( 'main-sidebar' ) ) dynamic_sidebar( 'main-sidebar' ); ?>

		</div>
	</div>

<?php get_footer(); ?>