<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php wp_title(); ?> - <?php bloginfo('name'); ?></title>

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">

	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
	<meta property="og:title" content="<?php bloginfo('name'); ?>" />
	<meta property="og:description" content="<?php bloginfo('description'); ?>" />
	<meta property="og:url" content="<?php bloginfo('home'); ?>" />
	<!-- <meta property="og:image" content="https://w3inova.com/img/responsive.jpg" /> -->

    <?php wp_head(); ?>

</head>
<body>

	<div class="z-depth-1">
		<header>
			<div class="container">

				<nav class="z-depth-0">
					<div class="nav-wrapper">
						<a href="<?php echo site_url('/') ?>" class="brand-logo"><?php bloginfo('name'); ?></a>
						<ul id="nav-mobile" class="right hide-on-med-and-down">
							<li>
								<a href="<?php echo site_url('/') ?>">Início</a>
							</li>
							<li>
								<a href="<?php echo getAppLink() ?>">&larr; Voltar para o App</a>
							</li>
						</ul>
					</div>
				</nav>

			</div>
		</header>
		<div class="search">
			<div class="container">
				<?php get_template_part('search-form'); ?>
			</div>
		</div>
	</div>

	<div class="container">
