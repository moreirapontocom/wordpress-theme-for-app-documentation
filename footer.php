
	</div>
	<!-- /.container -->

	<footer>
		<div class="container">
			&copy; <?php echo date('Y'); ?>. <a href="<?php echo getAppLink() ?>"><?php bloginfo('name'); ?></a>.
		</div>
	</footer>

	<script>
		// GA
	</script>

	<?php wp_footer(); ?>

</body>
</html>
