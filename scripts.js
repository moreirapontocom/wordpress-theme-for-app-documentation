jQuery(function() {
    $('.materialboxed').materialbox();
    $('.wp-caption').removeAttr('style');
    $('.widget ul').addClass('z-depth-1');
});